<!DOCTYPE html>
<html lang="en">
<head>
  <title>Movie Trailers</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo base_url(); ?>/assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>

<nav class="navbar navbar-expand-xl bg-dark navbar-dark">
  <a href="#" class="navbar-brand"><i class="fa fa-cube"></i>Movie<b>Trailers</b></a>
  <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
    <span class="navbar-toggler-icon"></span>
  </button>
  <!-- Collection of nav links, forms, and other content for toggling -->
  <div id="navbarCollapse" class="collapse navbar-collapse justify-content-start">
    <div class="navbar-nav">
       <?php if (session('logged_in')) : ?>
         <a href="<?php echo site_url("home"); ?>" class="nav-item nav-link">Home</a>
       <a href="<?php echo site_url("movies/index"); ?>" class="nav-item nav-link">Movies</a>
   </div>

    <div class="navbar-nav ml-auto">
      <div class="nav-item dropdown">
        <a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle user-action">Logged in as :  <?php echo session('firstname') . ' '. session('lastname'); ?> <b class="caret"></b></a>
        <div class="dropdown-menu">
          <a href="<?php echo site_url("login/account"); ?>"class="dropdown-item"><i class="fa fa-user-o"></i> My Account</a></a>
          <div class="dropdown-divider"></div>
          <a href="<?php echo site_url("login/logout"); ?>" class="dropdown-item"><i class="material-icons">&#xE8AC;</i> Logout</a></a>
        </div>
      </div>
    </div>
   <?php endif; ?>
 
  </div>
</nav>
<br>
<?php if (session()->get('success')): ?>
          <div class="alert alert-success" role="alert">
            <?= session()->get('success') ?>
          </div>
        <?php endif; ?>
      
</body>
</html>
