<div class="container">
  <div class="row">
    <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 mt-5 pt-3 pb-3 bg-white from-wrapper">
      <a class="dropdown-item" href="<?= site_url('lang/en'); ?>">English</a>
      <a class="dropdown-item" href="<?= site_url('lang/sw'); ?>">Swahili</a>
      <div class="container">
        <h3><?= lang("Text.page_title") ?></h3>
       
      <hr>
        <form action="<?php echo site_url("login/auth"); ?>" method="post" >
           <?php if (session()->get('msg')): ?>
          <div class="alert alert-danger" role="alert">
            <?= session()->get('msg') ?>
          </div>
        <?php endif; ?> 
         <div class="form-group">
           <label for="email"><?= lang("Text.Email_Address") ?></label>
           <input type="text" class="form-control" name="email" id="email" value="">
            <?php if (isset($validation)): ?>
            <!-- Error -->
        <?php if($validation->getError('email')) {?>
            <div class='alert alert-danger mt-2'>
              <?= $error = $validation->getError('email'); ?>
            </div>
        <?php }?>
      <?php endif; ?>
          </div>

          <div class="form-group">
           <label for="password"><?= lang("Text.Password") ?></label>
           <input type="password" class="form-control" name="password" id="password" value="">
            <?php if (isset($validation)): ?>
           <!-- Error -->
            <?php if($validation->getError('password')) {?>
                <div class='alert alert-danger mt-2'>
                  <?= $error = $validation->getError('password'); ?>
                </div>
            <?php }?>
          <?php endif; ?>
          </div>
         
          <div class="row">
            <div class="col-12 col-sm-4">
              <button type="submit" class="btn btn-primary"><?= lang("Text.Login") ?></button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>