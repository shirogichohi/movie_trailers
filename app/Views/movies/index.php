<div class="container">
	<div style="float: right; margin-bottom: 5px;">
		<a  class="btn btn-primary" href="<?php echo site_url("movies/add"); ?>" >>Add New Movie</a>
	</div>
 	<table class="table table-striped">
    <thead>
      <tr>
      	<th>No</th>
        <th>Title</th>
        <th>Director</th>
        <th>Language</th>
        <th>Release Year</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
    	<?php $counter = 1; if(is_array($movies)) :?>
    	<?php foreach($movies as $movie): ?>
      <tr>
      	<td><?php echo $counter+1;  ?></td>
        <td><?php echo $movie['title']; ?></td>
        <td><?php echo $movie['director']; ?></td>
       <td><?php echo $movie['language']; ?></td>
       <td><?php echo $movie['release_year']; ?></td>
       <td><a href="<?php echo site_url("movies/edit/" .$movie['id']) ?>"><i class="fa fa-edit"></i></a>
       <a href="<?php echo site_url("movies/delete/".$movie['id']); ?>"> <i class="fa fa-trash"></i></a></td>
      </tr>
  <?php endforeach; endif; ?>
     </tbody>
 </table>
	</div>