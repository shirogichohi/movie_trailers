<div class="container col-sm-6" >
  <?php if(\Config\Services::validation()->listErrors()) { ?>
<div class="alert alert-warning" role="alert">
<?= \Config\Services::validation()->listErrors(); ?>
  </div>
<?php }  ?>
	<form action="<?php echo site_url("movies/add"); ?>" method="post" >
  <div class="form-group">
    <label for="title">Title:</label>
    <input name="title" type="text" class="form-control" placeholder="Enter Movie Title" id="title">
  </div>
   <div class="form-group">
    <label for="director">Director:</label>
    <input name="director" type="text" class="form-control" placeholder="Enter Movie Diretor" id="director">
  </div>
   <div class="form-group">
    <label for="language">Language:</label>
    <input name="language" type="text" class="form-control" placeholder="Lannguage" id="language">
  </div>
   <div class="form-group">
    <label for="year">Year of Release:</label>
    <input name="year" type="text" class="form-control" placeholder="Year of Release" id="year">
  </div>
 
  <button type="submit" class="btn btn-primary">Save</button>
</form>
        </div>
        