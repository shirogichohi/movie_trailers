<div class="container col-sm-6" >

<?= \Config\Services::validation()->listErrors(); ?>
<?php  if (!$movies || !is_array($movies)) {
                ?>
                <div class="notification note-error">
                    <span class="icon"></span>
                    <p> &nbsp; Specified Movie Trailer was not found in the database.</p>
                </div>
                <?php
                return false;
            }

        ?>
	<form action="<?php echo site_url("movies/edit"); ?>" method="post" >
  <div class="form-group">
    <label for="title">Title:</label>
    <input name="title" type="text" class="form-control" placeholder="Enter Movie Title" id="title" value="<?php  echo $movies['title']; ?>">
  </div>
   <div class="form-group">
    <label for="director">Director:</label>
    <input name="director" type="text" class="form-control" placeholder="Enter Movie Diretor" id="director" value="<?php  echo $movies['director']?>">
  </div>
   <div class="form-group">
    <label for="language">Language:</label>
    <input name="language" type="text" class="form-control" placeholder="Lannguage" id="language" value="<?php  echo $movies['language']?>">
  </div>
   <div class="form-group">
    <label for="year">Year of Release:</label>
    <input name="year" type="text" class="form-control" placeholder="Year of Release" id="year" value="<?php  echo $movies['release_year']?>">
  </div>
 <input name='id' type='hidden' value="<?php echo $id; ?>" />
  <button type="submit" class="btn btn-primary">Edit</button>
</form>
        </div>
        