<?php

namespace App\Models;

use CodeIgniter\Model;

class MoviesModel extends Model
{
    protected $table = 'movies';
    protected $allowedFields = ['title','director','language','release_year'];
}

 
