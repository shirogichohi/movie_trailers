<?php namespace App\Controllers;
 
use CodeIgniter\Controller;
use App\Models\MoviesModel;
 
class Movies extends BaseController
{

	public function index(){

		$model = new MoviesModel();
		$model_movies =  $model->where('active', 1)->findAll();
		echo view('templates/header');
		echo view('movies/index', ['movies' => $model_movies]);
		echo view('templates/footer');
	}

	public function add(){

		helper(['form', 'url']);
	 if ($this->request->getMethod() == 'post') {
        $validate = $this->validate([
            'title' => 'required|min_length[3]',
            'director' => 'required',
            'language' => 'required',
            'year' => 'required|numeric'
        ]);

        $formModel = new MoviesModel();
 
        if (!$validate) {
        	echo view('templates/header');
            echo view('movies/add', [
                'validation' => $this->validator
            ]);
            session()->setFlashdata('error', $this->validator);
            echo view('templates/footer');
        } else {
            $formModel->save([
                'title' => $this->request->getVar('title'),
                'director'  => $this->request->getVar('director'),
                'language'  => $this->request->getVar('language'),
                'release_year'  => $this->request->getVar('year'),
            ]);          
            return $this->response->redirect(site_url('/movies/index'));
        }
    
}else {
	echo view('templates/header');
	echo view('movies/add');
	echo view('templates/footer');
	}

}

public function edit($id = 0)
{
	 $model = new MoviesModel();
	 if ($this->request->getMethod() == 'get') {
	 	if (!is_numeric($id) || $id == 0) {
            redirect(site_url('/movies/index'));
            return TRUE;
        }
	 }

     helper(['form', 'url']);
	 if ($this->request->getMethod() == 'post') {
        $validate = $this->validate([
            'title' => 'required|min_length[3]',
            'director' => 'required',
            'language' => 'required',
            'year' => 'required|numeric'
        ]);

        if (!$validate) {
        	echo view('templates/header');
            echo view('movies/add', [
                'validation' => $this->validator
            ]);
            echo view('templates/footer');
        } else {
         $data_to_update = [
                 'title' => $this->request->getVar('title'),
                'director'  => $this->request->getVar('director'),
                'language'  => $this->request->getVar('language'),
                'release_year'  => $this->request->getVar('year'),
            ];
           $result = $model->update((int)$this->request->getVar('id'), $data_to_update);
			return redirect()->to('movies/index');
		}

} else {
	
			$data['movies'] = $model->where('id', $id)->first();
			echo view('templates/header');
			echo view('movies/edit', $data);
			echo view('templates/footer');
}


}

}