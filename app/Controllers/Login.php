<?php namespace App\Controllers;
 
use CodeIgniter\Controller;
use App\Models\UsersModel;
 
class Login extends BaseController
{

  public function index()
    {
        helper(['form']);
        echo view ('templates/header');
        echo view('login');

		echo view ('templates/footer');
    } 
 
    public function auth()
    {
     if ($this->request->getMethod() == 'post') {
    	//validate
    		$rules = [
				'email' => 'required|valid_email',
				'password' => 'required',
			];
	
		if (!$this->validate($rules)) {
			echo view('templates/header');
			echo view('/login', [
                   'validation' => $this->validator
            ]);
            echo view('templates/footer');
        }
	    $model = new UsersModel();
        $email = $this->request->getVar('email');
        $password = $this->request->getVar('password');
        $data = $model->where(['email'=> $email, 'password' => md5($password)])->first();
        if($data){
        	 $session = session();
             $ses_data = [
                    'user_id'       => $data['id'],
                    'firstname'     => $data['firstname'],
                    'lastname'     => $data['lastname'],
                    'email'    => $data['email'],
                    'logged_in'     => TRUE
                ];
                $session->set($ses_data);
                return redirect()->to('/home');
            }else{
            	session()->setFlashdata('msg', 'Wrong Credentials passsed');
               return redirect()->to('/login');
            }
        }
   
    }


    


 
    public function logout()
    {
        $session = session();
        $session->destroy();
        return redirect()->to('/login');
    }

    public function account(){
		
		$data = [];
		helper(['form']);
		$model = new UsersModel();

		if ($this->request->getMethod() == 'post') {
			//validate
			$rules = [
				'firstname' => 'required|min_length[3]',
				'lastname' => 'required|min_length[3]',
				'phone' => 'required|numeric'
				];

			if($this->request->getPost('password') != ''){
				$rules['password'] = 'required|min_length[8]|max_length[255]';
				$rules['password_confirm'] = 'matches[password]';
			}


			if (! $this->validate($rules)) {
				$data['validation'] = $this->validator;
				echo view('login/account', $data);
			}else{

				$newData = [
					'id' => session()->get('user_id'),
					'firstname' => $this->request->getPost('firstname'),
					'lastname' => $this->request->getPost('lastname'),
					'phone' => $this->request->getPost('phone'),
					'active' => $this->request->getPost('active'),
					];
					if($this->request->getPost('password') != ''){
						$newData['password'] = md5($this->request->getPost('password'));
					}
				$model->save($newData);

				session()->setFlashdata('success', 'Successfuly Updated');
				return redirect()->to('login/account');

			}
		}

		$data['user'] = $model->where('id', session('user_id'))->first();
		echo view('templates/header', $data);
		echo view('account');
		echo view('templates/footer');
	}


   }