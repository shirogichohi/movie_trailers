/*
Navicat MySQL Data Transfer

Source Server         : fmt
Source Server Version : 50732
Source Host           : localhost:3306
Source Database       : movie_trailers

Target Server Type    : MYSQL
Target Server Version : 50732
File Encoding         : 65001

Date: 2021-04-01 09:55:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for movies
-- ----------------------------
DROP TABLE IF EXISTS `movies`;
CREATE TABLE `movies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `director` varchar(100) DEFAULT NULL,
  `language` varchar(100) DEFAULT NULL,
  `release_year` varchar(50) DEFAULT NULL,
  `active` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of movies
-- ----------------------------
INSERT INTO `movies` VALUES ('1', 'Paradise', 'Denquire', 'English', '2020', '1');
INSERT INTO `movies` VALUES ('2', 'Preachable', 'dendiue', 'Spanish', '2013', '1');
INSERT INTO `movies` VALUES ('3', 'Shefa', 'drena drena', 'French', '2003', '1');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `phone` int(10) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `active` tinyint(2) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'mary', 'mary', null, 'male', 'marygichohi@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '1');
INSERT INTO `users` VALUES ('2', 'admin', 'admin', '720111654', null, 'movieadmin@gmail.com', '21232f297a57a5a743894a0e4a801fc3', '1');
